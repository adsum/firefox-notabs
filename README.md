Firefox No Tabs
===============

This Firefox Web Extensions completely disables browser tabs,
by *always* opening a new window instead,
except for the *Customize Firefox* tab (see issue #2).

Usage
-----
Just install it. There is no configuration, because there is only one feature.

To hide the tab bar, check out https://www.reddit.com/r/firefox/comments/48wnba/css_code_to_hide_the_tabbar/

Permissions
-----------
- **tabs**: Required to access the tab title, to determine if a tab is the "Customize Firefox" tab (see issue #2)

License
-------
Firefox-notabs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firefox-notabs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with firefox-notabs.  If not, see <http://www.gnu.org/licenses/>.
