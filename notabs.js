/*
 * This file is part of firefox-notabs.
 *
 * firefox-notabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * firefox-notabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with firefox-notabs.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Enforce that the given tab is in a window containing no other tabs,
 * by moving it to a new one if necessary
 */
function enforceTabIsAlone(tab) {
    browser.tabs
        .query({ windowId: tab.windowId })
        .then(function(tabsInWindow) {
            if (tabsInWindow.length > 1
                && !(tab.url == "about:blank"
                     && tab.title == "Customize Firefox")) {
                // Tab opened in existing window => Move to new
                browser.windows.create({
                    tabId: tab.id
                }).then(function(newWindow) {
                    // Focus new window, if tab is supposed to be active
                    browser.windows.update(newWindow.id, {
                        focused: tab.active
                    });
                });
            }
        });
}

// Always enforce that a tab is alone, when one is created
browser.tabs.onCreated.addListener(enforceTabIsAlone);
